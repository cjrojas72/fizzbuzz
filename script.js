function fizzbuzz(maxValue) {
  let fizzString = " ";
  for (let i = 1; i < maxValue; i++) {
    if (i % 2 === 0 && i % 3 === 0) {
      fizzString = fizzString.concat("FizzBuzz, ");
    } else if (i % 2 === 0) {
      fizzString = fizzString.concat("Fizz, ");
    } else if (i % 3 === 0) {
      fizzString = fizzString.concat("Buzz, ");
    } else {
      fizzString = fizzString.concat(i + ", ");
    }
  }
  console.log(fizzString);
}

fizzbuzz(20);
